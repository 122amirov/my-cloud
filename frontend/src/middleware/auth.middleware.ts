export default async function ({next, store}:any):Promise<any> {
    await store.dispatch("auth")
    if (!store.state.auth.isAuth){
        return next({
            name:"Login"
        })
    }
    return next()
}
