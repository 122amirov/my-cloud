import {createApp} from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import '@/assets/tailwind.css'
import Toast, { PluginOptions } from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";
const options: PluginOptions = {
	// You can set your default options here
};

createApp(App).use(store).use(router).use(Toast,options).mount('#app')
