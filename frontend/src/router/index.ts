import {createRouter, createWebHistory, NavigationGuardNext, RouteLocationNormalized, RouteRecordRaw} from 'vue-router'
import Home from '../views/Home.vue'
import authMiddleWare from "@/middleware/auth.middleware";
import store from "@/store"

const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'Home',
		component: Home,
		meta: {
			middleware: [authMiddleWare]
		}
	},{
		path: '/profile',
		name: 'Profile',
		component: ()=>import('@/views/Profile.vue'),
		meta: {
			middleware: [authMiddleWare]
		}
	},
	{
		path: '/auth/register',
		name: 'Register',
		component: () => import('@/views/Auth/AuthRegister.vue'),
		meta: {
			layout: 'auth'
		}
	},
	{
		path: '/auth/login',
		name: 'Login',
		component: () => import('@/views/Auth/AuthLogin.vue'),
		meta: {
			layout: 'auth'
		}
	},

]

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes
})

router.beforeEach((to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
	if (!to.meta.middleware) {
		return next()
	}
	const middleware: any = to.meta.middleware
	const context = {
		to,
		from,
		next,
		store
	}
	return middleware[0]({...context})
})

export default router
