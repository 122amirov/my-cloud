export interface User {
	password:string,
	email:string
}

export interface NewUser extends User{
	login:string
}

export interface AuthorizedUser {
	diskSpace:number,
	email:string,
	avatar?:string
	id:string,
	usedSpace:number
}

export interface PayloadUser {
	token:string,
	user: AuthorizedUser | null
}
