export interface LinkInterface {
    id:string,
    route:string,
    title:string
}
