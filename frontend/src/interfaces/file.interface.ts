export interface FileInterface {
	readonly _id: string,
	name: string,
	type: string,
	size: number | null,
	path: string,
	parent: string,
	childs: string[],
	date: string
}
