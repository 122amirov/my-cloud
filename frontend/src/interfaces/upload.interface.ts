export interface UploadInterface{
	id: any,
	progress: null | number,
	name:string,
	size:number
}
