import {AuthorizedUser} from "@/interfaces/auth.interface";
import {FileInterface} from "@/interfaces/file.interface";
import {UploadInterface} from "@/interfaces/upload.interface";
import {SelectInterface} from "@/interfaces/select.interface";

export interface AuthModuleState {
	user: null | AuthorizedUser,
	isAuth: boolean
}

export interface FileModuleState {
	files: null | Array<FileInterface>,
	parents: Array<FileInterface>,
	progress?: null | number,
	deletingFile: null | FileInterface,
	uploadFiles: Array<UploadInterface>,
	uploadVisible: boolean
}

export interface SortModuleState {
	value: string,
	options: SelectInterface[]
}

export interface State {
	auth: AuthModuleState,
	file: FileModuleState,
	sort:SortModuleState
}
