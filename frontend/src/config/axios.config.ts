import axios from "axios";
import {API_URL} from "@/config/base.config";
export const http = axios.create({
	baseURL:`${API_URL}api/`
})
