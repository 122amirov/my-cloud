export const getIcons = () => ([
	{id: 'dir', icon: 'folder-icon', color: 'text-yellow-500'},
	{id: 'txt', icon: 'document-text-icon', color: 'text-gray-500'},
	{id: 'pdf', icon: 'document-text-icon', color: 'text-red-500'},
	{id: 'docx', icon: 'document-text-icon', color: 'text-blue-500'},
	{id: 'png', icon: 'photograph-icon', color: 'text-pink-500'},
	{id: 'jpeg', icon: 'photograph-icon', color: 'text-pink-500'},
	{id: 'webp', icon: 'photograph-icon', color: 'text-pink-500'},
	{id: 'svg', icon: 'photograph-icon', color: 'text-pink-500'},
	{id: 'jpg', icon: 'photograph-icon', color: 'text-pink-500'},
	{id: 'zip', icon: 'archive-icon', color: 'text-red-800'},
])
