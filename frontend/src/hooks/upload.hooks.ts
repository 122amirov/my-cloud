import {useStore} from "vuex";
import {computed} from "vue";
import {UploadInterface} from "@/interfaces/upload.interface";

export const useUpload = () => {
	const store = useStore()
	const getVisibleUpload = computed(() => store.state.file.uploadVisible)
	const getUploadFiles = computed<UploadInterface[]>(() => store.state.file.uploadFiles)
	const removeUploadFileHandler = (file: UploadInterface) => {
		store.commit('REMOVE_UPLOAD_FILE', file)
	}
	const closeUploadHandler = () => {
		store.commit("CLEAR_UPLOAD_FILE")
		store.commit("SHOW_UPLOAD", false)
	}
	return {
		getVisibleUpload,
		getUploadFiles,
		removeUploadFileHandler,
		closeUploadHandler
	}
}
