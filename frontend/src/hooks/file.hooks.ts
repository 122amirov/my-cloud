import {useStore} from "vuex";
import {computed, ref} from "vue";
import {FileInterface} from "@/interfaces/file.interface";
import {useToast} from "vue-toastification"

export const useFile = () => {
	const store = useStore()

	const loading = ref<boolean>(false)

	const currentDir = computed((): FileInterface => {
		return store.state.file.parents && store.state.file.parents[store.state.file.parents.length - 1]
	})

	const getFiles = computed((): FileInterface => store.state.file.files)

	const downloadHandler = async (file: FileInterface): Promise<void> => {
		await store.dispatch('downloadFile', file)
	}

	return {getFiles, currentDir, loading, downloadHandler}
}

export const useOptions = () => {
	const store = useStore()
	const goBackHandler = () => {
		store.commit("POP_PARENTS")
	}

	const changeParentHandler = (file: FileInterface) => {
		store.commit("SET_PARENTS", file)
	}

	return {goBackHandler, changeParentHandler}
}

export const useUploadFile = () => {
	const store = useStore()
	const loadingBtn = ref(false)
	const {currentDir} = useFile()

	const uploadHandler = async (event: Event) => {
		try {
			const files = [...(event.target as HTMLInputElement).files || []]
			const currentDirId = currentDir.value?._id
			if (!files.length) {
				return
			}
			store.commit("SHOW_UPLOAD", true)
			for (const file of files) {
				const payload = {
					currentDirId,
					file
				}
				await store.dispatch("uploadFile", payload)
			}
			await store.dispatch("fetchFiles", currentDirId)
		} catch (e) {
			console.log(e)
		} finally {
			loadingBtn.value = false
		}
	}
	return {
		uploadHandler,
	}
}

export const useDeleteFile = () => {
	const store = useStore()
	const {currentDir} = useFile()
	const deleteModal = ref(false)
	const loadingBtn = ref(false)
	const toast = useToast()
	const getDeletingFile = computed<FileInterface>(() => store.state.file.deletingFile)
	const acceptDeleteHandler = (file: FileInterface) => {
		store.commit('SET_DELETING_FILE', file)
		deleteModal.value = true
	}
	const deleteHandler = async () => {
		try {
			loadingBtn.value = true
			await store.dispatch('deleteFile', getDeletingFile.value?._id)
			await store.dispatch('fetchFiles', currentDir.value?._id)
			deleteModal.value = false
			toast.success(`${store.state.file.deletingFile.name} is deleted`)
			store.commit('SET_DELETING_FILE', null)
		} catch (e) {
			toast.error(e.response.data?.message)
		} finally {
			loadingBtn.value = false
		}
	}
	return {deleteHandler, deleteModal, getDeletingFile, acceptDeleteHandler, loadingBtn}
}
