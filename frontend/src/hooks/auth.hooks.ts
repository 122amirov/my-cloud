import {ref} from "vue";
import {useRouter} from "vue-router";
import {useStore} from "vuex";
import {useToast} from "vue-toastification"

export const useAuth = () => {
	const email = ref<string>("")
	const password = ref<string>("")
	const login = ref<string>("")
	const repeatPassword = ref<string>("")
	const router = useRouter()
	const store = useStore()
	const toast = useToast()

	const signInHandler = async (): Promise<any> => {
		try {
			if (!email.value || !password.value) {
				return
			}
			const payload = {
				email: email.value,
				password: password.value
			}
			await store.dispatch('login', payload)
			await router.push('/')
			toast.success("You successfully sign in", {
				timeout: 2000
			})
		}catch (e) {
			toast.error(e.response.data.message, {
				timeout: 3000
			})
		}
	}

	const signUpHandler = async (): Promise<any> => {
		try {
			if (!email.value || !password.value || !repeatPassword.value || password.value !== repeatPassword.value) {
				return
			}
			const payload = {
				email: email.value,
				password: password.value
			}
			await store.dispatch("register", payload)
			await router.push('/auth/login')
			toast.success("You successfully sign up", {
				timeout: 2000
			})
		} catch (e) {
			toast.error(e.response.data.message, {
				timeout: 2000
			})
		}
	}

	return {email, password, login, repeatPassword, signInHandler, signUpHandler}
}
