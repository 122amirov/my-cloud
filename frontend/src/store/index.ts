import {createStore} from 'vuex'
import auth from '@/store/modules/auth'
import file from '@/store/modules/file'
import sort from '@/store/modules/sort'
import {State} from "@/interfaces/vuex.interface";
import {reloadUserPlugin} from "@/store/plugins/reload.user.plugin";

export default createStore<State>({
	modules: {
		auth,
		file,
		sort
	},
	plugins: [reloadUserPlugin]
})
