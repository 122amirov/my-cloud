import {MutationPayload, Store} from "vuex";
import {State} from "@/interfaces/vuex.interface";

export const reloadUserPlugin = async (store: Store<State>): Promise<void> => {
	store.subscribe(async (mutation: MutationPayload, state: State) => {
		if (mutation.type === "SET_FILES") {
			await store.dispatch("auth")
		}
	})
}
