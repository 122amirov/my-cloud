import {SortModuleState} from "@/interfaces/vuex.interface";

export const mutations = {
	SET_SORT_VALUE(state: SortModuleState, value: string) {
		state.value = value
	}
}
