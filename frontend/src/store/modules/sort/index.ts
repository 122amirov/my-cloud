import {SortModuleState, State} from "@/interfaces/vuex.interface";
import {ActionContext} from "vuex";
import {state} from "@/store/modules/sort/state";
import {mutations} from "@/store/modules/sort/mutations";


export type SortContext = ActionContext<SortModuleState, State>

export default {
	state,
	mutations
}
