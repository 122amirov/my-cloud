import {SortModuleState} from "@/interfaces/vuex.interface";

export const state = (): SortModuleState => ({
	value: 'name',
	options: [
		{id: 'name', label: 'Name'},
		{id: 'type', label: 'Type'},
		{id: 'date', label: 'Date'},
	],
})
