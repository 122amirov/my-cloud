import {AuthModuleState, State} from "@/interfaces/vuex.interface";
import {ActionContext} from "vuex";
import {state} from "@/store/modules/auth/state";
import {actions} from "@/store/modules/auth/actions";
import {mutations} from "@/store/modules/auth/mutations";

export type Context = ActionContext<AuthModuleState, State>

export default {
	state,
	mutations,
	actions
}
