import {NewUser, User} from "@/interfaces/auth.interface";
import {http} from "@/config/axios.config";
import {Context} from "@/store/modules/auth/index";

export const actions = {
	async login({commit}: Context, {email, password}: User): Promise<void> {
		const response = await http.post(`auth/login`, {
			email,
			password,
		});
		commit("SET_AUTH", response.data)
	},
	async register({}: Context, {email, password}: NewUser): Promise<void> {
		await http.post(`auth/register`, {
			email,
			password
		});
	},
	async auth({commit}: Context): Promise<void> {
		try {
			const response = await http.get('auth/auth', {
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			commit("SET_AUTH", response.data)
		} catch (e) {
			commit('CLEAR_AUTH')
			console.log(e)
		}
	},

	async uploadAvatar({commit}: Context, file: File): Promise<void> {
		const formData = new FormData()
		formData.append("file", file)
		const response = await http.post("files/avatar", formData, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		commit("SET_USER", response.data)
	},
	async deleteAvatar({commit}: Context): Promise<void> {
		const response = await http.delete("files/avatar",{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		commit("SET_USER", response.data)
	}
}
