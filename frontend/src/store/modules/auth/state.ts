import {AuthModuleState} from "@/interfaces/vuex.interface";

export const state = (): AuthModuleState => ({
	user: null,
	isAuth: false,
})
