import {AuthModuleState} from "@/interfaces/vuex.interface";
import {AuthorizedUser, PayloadUser} from "@/interfaces/auth.interface";

export const mutations = {
	SET_AUTH(state: AuthModuleState, payload: PayloadUser): void {
		state.user = payload.user;
		localStorage.setItem("token", payload.token)
		state.isAuth = true
	},
	CLEAR_AUTH(state: AuthModuleState): void {
		state.user = null
		state.isAuth = false
		localStorage.removeItem("token")
	},
	SET_USER(state: AuthModuleState, user: AuthorizedUser): void {
		state.user = user
	},
}
