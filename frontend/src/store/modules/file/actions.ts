import {http} from "@/config/axios.config";
import {UploadInterface} from "@/interfaces/upload.interface";
import {FileInterface} from "@/interfaces/file.interface";
import {FileContext} from "@/store/modules/file/index";

interface NewFolderPayload {
	name: string,
	type: string
}

interface UploadFilePayload {
	currentDirId: string,
	file: File
}

export const actions = {
	fetchFiles: async ({commit, state, rootState}: FileContext, parent: string): Promise<void> => {
		console.log(rootState)
		const response = await http.get("files", {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			},
			params: {
				"parent": parent,
				"sort": rootState.sort.value
			}
		})
		commit("SET_FILES", response.data)
	},
	createFolder: async ({state}: FileContext, payload: NewFolderPayload): Promise<void> => {
		const currentDirectoryId = state.parents[state.parents.length - 1]?._id
		const newFolder = {
			...payload,
			parent: currentDirectoryId
		}
		await http.post("files", newFolder, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			},
		})
	},
	uploadFile: async ({state, commit}: FileContext, {file, currentDirId}: UploadFilePayload) => {
		const form: FormData = new FormData()
		form.append("file", file)
		if (currentDirId) {
			form.append("parent", currentDirId)
		}
		commit("SHOW_UPLOAD", true)
		let fileUpload: UploadInterface = {name: file.name, size: file.size, id: Date.now(), progress: 0}
		commit("ADD_UPLOAD_FILES", fileUpload)
		await http.post("files/upload", form, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			},
			onUploadProgress: progressEvent => {
				const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
				fileUpload.progress = totalLength ? Math.round((progressEvent.loaded * 100) / totalLength) : null
				commit("CHANGE_UPLOAD_FILE", fileUpload)
			}
		})
	},
	downloadFile: async ({}: FileContext, file: FileInterface): Promise<void> => {
		const response = await http.get('files/download', {
			responseType: 'blob',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			},
			params: {
				'id': file._id
			}
		})
		const url = window.URL.createObjectURL(new Blob([response.data]));
		const link = document.createElement('a');
		link.href = url;
		link.download = file.name
		document.body.appendChild(link);
		link.click();
		link.remove()
	},
	deleteFile: async ({}: FileContext, id: string): Promise<void> => {
		await http.delete('files/delete', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			},
			params: {
				'id': id
			}
		})
	},
	searchFiles: async ({commit}: FileContext, search: string): Promise<void> => {
		const response = await http.get('files/search', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
			},
			params: {
				"text": search
			}
		})
		commit("SET_FILES", response.data)
		commit("CLEAR_PARENTS")
	}
}
