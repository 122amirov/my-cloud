import {FileModuleState} from "@/interfaces/vuex.interface";

export const state = (): FileModuleState => ({
	files: null,
	parents: [],
	deletingFile: null,
	uploadFiles: [],
	uploadVisible: false
})
