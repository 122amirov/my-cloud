import {FileModuleState} from "@/interfaces/vuex.interface";
import {FileInterface} from "@/interfaces/file.interface";
import {UploadInterface} from "@/interfaces/upload.interface";

export const mutations = {
	SET_FILES: (state: FileModuleState, payload: FileInterface[]) => {
		state.files = [...payload]
	},
	POP_PARENTS: (state: FileModuleState) => {
		const lastParent = state.parents[state.parents.length - 1]
		state.parents = [...state.parents.filter(item => item._id !== lastParent._id)]
	},
	CLEAR_PARENTS: (state: FileModuleState) => {
		state.parents = []
	},
	SET_PARENTS: (state: FileModuleState, payload: FileInterface) => {
		state.parents = [...state.parents, payload]
	},
	SET_DELETING_FILE: (state: FileModuleState, payload: FileInterface) => {
		state.deletingFile = payload
	},
	SHOW_UPLOAD: (state: FileModuleState, payload: boolean) => {
		state.uploadVisible = payload
	},
	CLEAR_UPLOAD_FILE: (state: FileModuleState) => {
		state.uploadFiles = []
	},
	ADD_UPLOAD_FILES: (state: FileModuleState, payload: UploadInterface) => {
		state.uploadFiles = [...state.uploadFiles, payload]
	},
	REMOVE_UPLOAD_FILE: (state: FileModuleState, payload: UploadInterface) => {
		state.uploadFiles = [...state.uploadFiles.filter(item => item.id !== payload.id)]
	},
	CHANGE_UPLOAD_FILE: (state: FileModuleState, payload: UploadInterface) => {
		state.uploadFiles = [...state.uploadFiles.map(item => item.id === payload.id
			? {...item, progress: payload.progress}
			: {...item}
		)]
	}
}
