import {FileModuleState, State} from "@/interfaces/vuex.interface";
import {ActionContext} from "vuex";
import {state} from "@/store/modules/file/state";
import {mutations} from "@/store/modules/file/mutations";
import {actions} from "@/store/modules/file/actions";

export type FileContext = ActionContext<FileModuleState, State>

export default {
	state,
	mutations,
	actions
}
