const User = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const fileService = require("../services/FileService")
const File = require("../models/file")

class AuthService {
    async register({email, password}) {
        const user = await User.findOne({email})
        if (user) {
            throw new Error(`User with email ${email} is already exist`)
        }
        const hashPassword = await bcrypt.hash(password, 5)
        const newUser = new User({email, password: hashPassword})
        await newUser.save()
        await fileService.createDirectory(new File({user: newUser._id, name: ''}))
        return {message: `User with email '${email}' is created`}
    }

    async login({email, password}) {
        const user = await User.findOne({email})
        if (!user) {
            throw new Error(`User with email ${email} not found`)
        }
        const isPassValid = await bcrypt.compareSync(password, user.password)
        if (!isPassValid) {
            throw new Error(`email or password incorrect`)
        }
        const token = jwt.sign({id: user.id}, config.get("secretKey"), {expiresIn: "1h"})
        return {
            token,
            user: {
                id: user.id,
                email: user.email,
                diskSpace: user.diskSpace,
                usedSpace: user.usedSpace,
                avatar: user.avatar
            }
        }
    }

    async checkAuth({id}) {
        const user = await User.findOne({_id: id})
        const token = jwt.sign({id: user.id}, config.get("secretKey"), {expiresIn: "1h"})
        return {
            token,
            user: {
                id: user.id,
                email: user.email,
                diskSpace: user.diskSpace,
                usedSpace: user.usedSpace,
                avatar: user.avatar
            }
        }
    }
}

module.exports = new AuthService
