const fileSystem = require('fs')
const config = require('config')

class FileService {
	createDirectory(file) {
		return new Promise((resolve, reject) => {
			const filePath = `${config.get("filePath")}\\${file.user}\\${file.path}`
			try {
				if (!fileSystem.existsSync(filePath)) {
					fileSystem.mkdirSync(filePath)
					return resolve({message: 'File was created'})
				}
				return resolve({message: "File already exist"})
			} catch (e) {
				console.log(e)
				reject({message: 'File Error'})
			}
		})
	}

	deleteFile(file) {
		const path = this.getPath(file)
		if (file.type === 'dir'){
			fileSystem.rmdirSync(path)
		}else {
			fileSystem.unlinkSync(path)
		}
	}

	getPath(file) {
		return `${config.get("filePath")}\\${file.user}\\${file.path}`
	}
}

module.exports = new FileService()
