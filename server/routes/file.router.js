const Router = require('express')
const router = new Router
const authMiddleware = require('../middleware/auth.middleware')
const fileController = require('../controller/FileController')

router.post("", authMiddleware, fileController.createDirectory)
router.get("", authMiddleware, fileController.getFiles)
router.post("/upload", authMiddleware, fileController.uploadFile)
router.get("/download", authMiddleware, fileController.downloadFile)
router.delete("/delete", authMiddleware, fileController.deleteFile)
router.get("/search", authMiddleware, fileController.searchFile)
router.post("/avatar", authMiddleware, fileController.uploadAvatar)
router.delete("/avatar", authMiddleware, fileController.deleteAvatar)

module.exports = router
