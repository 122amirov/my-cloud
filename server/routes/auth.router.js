const Router = require('express')
const {check} = require("express-validator")
const AuthController = require('../controller/AuthController')
const AuthMiddleware = require('../middleware/auth.middleware')
const router = new Router
router.post('/register',
	[
		check('email', 'incorrect email').isEmail(),
		check('password', 'Password must be longer than 3 and shorter than 12').isLength({min: 3, max: 12})
	], AuthController.register)
router.post('/login', AuthController.login)
router.get('/auth', AuthMiddleware, AuthController.checkAuth)
module.exports = router
