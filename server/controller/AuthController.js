const {validationResult} = require("express-validator");
const AuthService = require("../services/AuthService")

class AuthController {
    async register(req, res) {
        try {
            const errors = validationResult(req.body)
            if (!errors.isEmpty()) {
                return res.status(400).json({message: 'Incorrect request', errors})
            }
            const result = await AuthService.register(req.body)
            return res.json(result)
        } catch (e) {
            console.log(e)
            return res.status(400).json({message: e.message})
        }
    }

    async login(req, res) {
        try {
            const user = await AuthService.login(req.body)
            return res.json(user)
        } catch (e) {
            return res.status(500).json({message: e.message})
        }
    }

    async checkAuth(req, res) {
        try {
            const user = await AuthService.checkAuth(req.user)
            return res.json(user)
        } catch (e) {
            return res.status(500).json({message: e.message})
        }
    }
}

module.exports = new AuthController()
