const File = require('../models/file')
const User = require('../models/user')
const fileService = require("../services/FileService")
const config = require('config')
const fs = require("fs");
const uuid = require('uuid')

class FileController {
	async createDirectory(req, res) {
		try {
			const {name, type, parent} = req.body
			const file = new File({name, type, parent, user: req.user.id,})
			const parentFile = await File.findOne({_id: parent})
			if (!parentFile) {
				file.path = name
				const result = await fileService.createDirectory(file)
				if (result.message === "File already exist") {
					return res.status(400).json(result)
				}
			} else {
				file.path = `${parentFile.path}\\${file.name}`
				const result = await fileService.createDirectory(file)
				if (result.message === "File already exist") {
					return res.status(400).json(result)
				}
				parentFile.childs.push(file._id)
				await parentFile.save()
			}
			await file.save()
			return res.json(file)
		} catch (e) {
			console.log(e)
			return res.status(500).json({message: "Something went wrong"})
		}
	}

	async getFiles(req, res) {
		try {
			const {sort} = req.query
			let files
			switch (sort) {
				case "name":
					files = await File.find({user: req.user.id, parent: req.query.parent}).sort({name: 1})
					break
				case 'type':
					files = await File.find({user: req.user.id, parent: req.query.parent}).sort({type: 1})
					break
				case 'date':
					files = await File.find({user: req.user.id, parent: req.query.parent}).sort({date: 1})
					break
				default:
					files = await File.find({user: req.user.id, parent: req.query.parent})
					break
			}
			return res.json(files)
		} catch (e) {
			console.log(e)
			return res.status(500).json({message: "Get File Error"})
		}
	}

	async uploadFile(req, res) {
		const file = req.files.file
		const parent = await File.findOne({user: req.user.id, _id: req.body.parent})
		const user = await User.findOne({_id: req.user.id})
		if (user.usedSpace + file.size > user.diskSpace) {
			return res.status(400).json({message: "There no space on the disk"})
		}
		user.usedSpace = user.usedSpace + file.size
		let path = parent
			? `${config.get('filePath')}\\${user._id}\\${parent.path}\\${file.name}`
			: `${config.get('filePath')}\\${user._id}\\${file.name}`
		if (fs.existsSync(path)) {
			return res.status(400).json({message: "File is already exist"})
		}
		file.mv(path)
		const type = file.name.split('.').pop()
		const filePath = parent ? (parent.path + '\\' + file.name) : file.name
		const dbFile = new File({
			name: file.name,
			type,
			user: user._id,
			size: file.size,
			path: filePath,
			parent: parent && parent._id
		})
		await dbFile.save()
		if (parent) {
			parent.childs.push(dbFile._id)
		}
		parent && await parent.save()
		await user.save()
		return res.status(200).json(dbFile)
	}

	async downloadFile(req, res) {
		try {
			const file = await File.findOne({_id: req.query.id, user: req.user.id})
			const path = fileService.getPath(file)
			if (!fs.existsSync(path)) {
				return res.status(400).json({message: "Download error"})
			}
			return res.download(path, file.name)
		} catch (e) {
			console.log(e)
			res.status(500).json({message: 'Download error'})
		}
	}

	async deleteFile(req, res) {
		try {
			const file = await File.findOne({_id: req.query.id, user: req.user.id})
			const parent = await File.findOne({_id: file.parent})
			const user = await User.findOne({_id: req.user.id})
			if (!file) {
				return res.status(400).json({message: "File not found"})
			}
			fileService.deleteFile(file)
			if (parent) {
				parent.childs = [...parent.childs.filter(item => item !== file._id)]
				await parent.save()
			}
			user.usedSpace = user.usedSpace - file.size
			await file.remove()
			await user.save()
			return res.status(200).json({message: "File is deleted"})
		} catch (e) {
			console.log(e)
			return res.status(500).json({message: "File error"})
		}
	}

	async searchFile(req, res) {
		try {
			const {text} = req.query
			let files = await File.find({user: req.user.id})
			files = files.filter(item => item.name.toLowerCase().includes(text.toLowerCase()))
			return res.status(200).json(files)
		} catch (e) {
			console.log(e)
			return res.status(500).json({message: 'File Error'})
		}
	}

	async uploadAvatar(req, res) {
		try {
			const file = req.files.file
			const user = await User.findById(req.user.id)
			const avatarName = uuid.v4() + '.jpg'
			file.mv(`${config.get('staticPath')}\\${avatarName}`)
			user.avatar = avatarName
			await user.save()
			return res.status(200).json(user)
		} catch (e) {
			console.log(e)
			return res.status(500).json({message: 'Avatar Error'})
		}
	}

	async deleteAvatar(req, res) {
		try {
			const user = await User.findById(req.user.id)
			if (!user.avatar) {
				return res.status(400).json({message: 'Avatar is already empty'})
			}
			fs.unlinkSync(`${config.get("staticPath")}\\${user.avatar}`)
			user.avatar = null
			await user.save()
			return res.status(200).json(user)
		} catch (e) {
			console.log(e)
			return res.status(500).json({message: 'Avatar Error'})
		}
	}
}

module.exports = new FileController()
